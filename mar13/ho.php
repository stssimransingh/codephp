<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script src="jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
</head>
<body>
    <?php
        $data = file_get_contents('https://restcountries.eu/rest/v2/all');
        $phpdata = json_decode($data,true);
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div id="accordion">
                <?php
                    foreach($phpdata as $k => $d){                    
                ?>
                <div class="card">
                <div class="card-header">
                    <a class="card-link" data-toggle="collapse" href="#collapseOne<?php echo $k; ?>">
                    <?php echo $d['name']; ?>
                    </a>
                </div>
                <div id="collapseOne<?php echo $k; ?>" class="collapse show" data-parent="#accordion">
                    <div class="card-body">
                    <?php 
                    if(count($d['borders']) > 0){
                        foreach($d['borders'] as $dd){
                            echo $dd . "<br/>";
                        }
                    }else{
                        echo "No Borders";
                    }
                    ?>
                    </div>
                </div>
                </div>
                <?php 
                    }
                ?>
            </div>
            </div>
        </div>
    </div>
</body>
</html>