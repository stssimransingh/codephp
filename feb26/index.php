<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="css/bootstrap.css" rel="stylesheet" />
</head>
<body class="bg-dark">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 mx-auto bg-light p-5 mt-5">
                <h1 class="text-center">Login</h1>
                <hr />
                <form action="" method="post">
                    <label>Username</label>
                    <input type="text" class="form-control" name="user" />
                    <label>Password</label>
                    <input type="password" name="pass" class="form-control" />
                    <input type="submit" value="Login" class="btn btn-success mt-3" />
                </form>
                <?php 
                    if(isset($_POST['user'])){
                        if($_POST['user']=='admin' && $_POST['pass']=='admin'){
                            echo "<img src='happy.gif' style='width:100%;' />";
                        }else{
                            echo "<img src='sad.gif' style='width:100%;' />";
                        }
                    }
                ?>
            </div>
        </div>
    </div>
</body>
</html>