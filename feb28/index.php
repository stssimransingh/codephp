<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>
<body>
    <?php 
        // Associative
        // Multidimentional
        // Indexed

        $a = ['hello','there','djfhjksdh']; // Index
        echo $a[0];
        $b = ['hello',['there']];
        echo $b[1][0];
        $c = [
            'name' => 'Simran Singh', 
            'fname' => 'Ranjit Singh'
        ];
        $d = ['name' => 'Peeter','details' => ['fname' => 'James','mother' => 'donal']];

        $e = ['james',['name' => 'jema', ['Hello']]];

        echo "<pre>";
        print_r($e);
        echo "</pre>";
        echo $e[1][0][0];


        $p = ['myar',['name' => ['james']]];
        echo "<pre>";
        print_r($p);
        echo "</pre>";
        echo $p[1]['name'][0];
    ?>
</body>
</html>